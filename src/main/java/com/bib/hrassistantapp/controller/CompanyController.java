package com.bib.hrassistantapp.controller;

import com.bib.hrassistantapp.model.Company;
import com.bib.hrassistantapp.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/company/")
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @PostMapping("/save")
    public Company upsertCompanyDetails(@Valid @RequestBody Company company)
    {
        return companyService.insertCompany(company);
    }

    @GetMapping("/details")
    public List<Company> getCompanyDetails()
    {
        return companyService.getCompanyDetails();
    }
}
