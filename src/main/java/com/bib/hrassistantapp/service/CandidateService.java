package com.bib.hrassistantapp.service;

import com.bib.hrassistantapp.model.Candidate;
import com.bib.hrassistantapp.model.dto.ListOptionDto;
import com.bib.hrassistantapp.model.dto.PageOptionDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CandidateService {
    Candidate insertCandidate(Candidate candidate);

    Candidate updateCandidate(Candidate candidate);

    ResponseEntity<String> deleteCandidate(Long id);

    List<Candidate> getCandidatesEmail(ListOptionDto listOptionDto);

    Page<Candidate> getCandidateWithPaginationAndFilters(PageOptionDto pageOption);


}
