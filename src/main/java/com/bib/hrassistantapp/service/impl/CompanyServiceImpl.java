package com.bib.hrassistantapp.service.impl;

import com.bib.hrassistantapp.advice.EntityAlreadyExistException;
import com.bib.hrassistantapp.advice.EntityNotFoundException;
import com.bib.hrassistantapp.model.Candidate;
import com.bib.hrassistantapp.model.Company;
import com.bib.hrassistantapp.repository.CompanyRepository;
import com.bib.hrassistantapp.service.CompanyService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@EnableJpaAuditing
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public Company insertCompany(Company company) {
        try{
            return companyRepository.save(company);
        }catch (DataIntegrityViolationException e){
            throw new EntityAlreadyExistException("Company with name ["+company.getName()+"] already exist");
        }
    }

    @Override
    public Company updateCompany(Company company) {
        Optional<Company> oldCompany = companyRepository.findById(company.getId());
        if(!oldCompany.isPresent()){
            throw new EntityNotFoundException("Company with ID ["+company.getId()+"] not found");
        }
        company.setCreatedAt(oldCompany.get().getCreatedAt());
        return companyRepository.save(company);
    }

    @Override
    public List<Company> getCompanyDetails() {
        return companyRepository.findAll();
    }
}
