package com.bib.hrassistantapp.service;

import com.bib.hrassistantapp.model.Company;

import java.util.List;


public interface CompanyService {

    Company insertCompany(Company company);

    Company updateCompany(Company company);

    List<Company> getCompanyDetails();
}
