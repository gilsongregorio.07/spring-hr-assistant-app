package com.bib.hrassistantapp.model.dto;

import lombok.Data;

@Data
public class PageOptionDto extends ListOptionDto {
    Integer page;
    Integer pageSize;
}
