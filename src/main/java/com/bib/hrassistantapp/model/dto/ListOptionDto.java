package com.bib.hrassistantapp.model.dto;

import lombok.Data;

@Data
public class ListOptionDto {
    String positionFilter;
    String overallStatusFilter;
    String fullNameSearch;
}
