package com.bib.hrassistantapp.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@EnableJpaAuditing
@Table(name = "Candidate")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Candidate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false)
    @NotBlank(message = "Full Name is mandatory")
    private String fullName;

    @Column(nullable = false)
    @NotBlank(message = "Position is mandatory")
    private String position;

    @Column(nullable = false)
    @NotBlank(message = "Contact number is mandatory")
    private String contactNumber;

    @Column(nullable = false, unique = true)
    @Email(message = "Email must be valid")
    private String email;

    @Column(nullable = false)

    private Date dateEndorsed;

    @Column(name = "overall_status", nullable = false)
    private String overallStatus;

    @NotBlank(message = "Hiring manager is mandatory")
    private String hiringManager;

    private String paperScreeningStatus;

    private Date techInterviewSchedule;

    private String interviewResult;

    private String offerStatus;

    private Date offerDate;

    private Date onBoardingDate;

    private Boolean isRejectionEmailSent;

    @ManyToOne
    @JoinColumn(name = "company_id")
    @NotNull(message = "Company cannot be null")
    private Company company;

    private Date createdAt;

    private Date updatedAt;


    @PrePersist
    public void onPrePersist() {
        if (getCreatedAt() == null)
            setCreatedAt(new Date());
        if (getUpdatedAt() == null)
            setUpdatedAt(new Date());
    }

    @PreUpdate
    public void onPreUpdate() {
        if (getUpdatedAt() == null)
            setUpdatedAt(new Date());
    }

}
