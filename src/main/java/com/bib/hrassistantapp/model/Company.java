package com.bib.hrassistantapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity

public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(unique = true)
    @NotBlank(message = "Company name is required to fill up")
    private String name;

    @Column(columnDefinition = "MEDIUMTEXT")
    @NotBlank(message = "Company information is required to fill up")
    private String information;

    @NotBlank(message = "Company website is required to fill up")
    @URL
    private String website;

    @Column(columnDefinition = "MEDIUMTEXT")
    @NotBlank(message = "Company disclaimer is required to fill up")
    private String disclaimer;

    @Getter(AccessLevel.NONE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "company", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Candidate> candidates = new HashSet<>();

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    @JsonIgnore()
    private Date createdAt;

    @UpdateTimestamp
    @JsonIgnore
    private Date updatedAt;


    public Set<Candidate> getCandidates() {
        return candidates;
    }
}
