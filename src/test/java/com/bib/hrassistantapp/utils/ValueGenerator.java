package com.bib.hrassistantapp.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ValueGenerator {
    public static String website(){
        List<String> websiteUrls = Arrays.asList("https://www.bibvip.com", "bib.com");

        return websiteUrls.get(generateRandomIndex(websiteUrls.size()));
    }

    public static String companyName(){
        List<String> websiteUrls = Arrays.asList("BIB", "Google", "");

        return websiteUrls.get(generateRandomIndex(websiteUrls.size()));
    }


    public static int generateRandomIndex(int size){
        int index = (int)(Math.random() * size);
        return index;
    }
}
