package com.bib.hrassistantapp.testcontroller;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String message){
        super(message);
    }
}
